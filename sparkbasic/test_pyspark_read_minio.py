from pyspark.sql import SparkSession
from pyspark.conf import SparkConf
from unittest import TestCase


class PyFuncModelWrapper(object):
    def __init__(self, spark, spark_model):
        self.spark = spark
        self.spark_model = spark_model

    def predict(self, pandas_df):
        spark_df = self.spark.createDataFrame(pandas_df)
        return [x.prediction for x in
                self.spark_model.transform(spark_df).select("prediction").collect()]

class testPySparkReadMinio(TestCase):
    def setUp(self):
        conf = SparkConf()
        conf.setMaster("local")
        conf.set("spark.hadoop.fs.s3a.endpoint", "http://10.10.62.180:39000")
        conf.set("spark.hadoop.fs.s3a.access.key", "minio")
        conf.set("spark.hadoop.fs.s3a.secret.key", "minio123")
        conf.set("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
        # conf.set("spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.7.7")
        self.spark = SparkSession.builder.config(conf=conf).getOrCreate()
        self.spark.sparkContext.setLogLevel('info')

    def tearDown(self):
        self.spark.stop()

    def _load_model(model_path):
        from pyspark.ml.pipeline import PipelineModel
        model = PipelineModel.load(model_path)
        return model

    # pyspark读取基本json数据
    def test_pyspark_read_minio(self):
        # df = self.spark.read.format('csv').load('s3a://models/iris_test.csv')
        df = self.spark.read.csv(path='s3a://models/iris_test.csv', header="true")
        count = df.show()

    # pyspark读取模型
    def test_load_model_minio(self):
        return PyFuncModelWrapper(self.spark, self._load_model("s3a://models/"))


