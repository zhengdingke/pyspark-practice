
import sys
import os
from operator import add

from pyspark.sql import SparkSession
from pyspark.conf import SparkConf

if __name__ == '__main__':
    os.environ["PYSPARK_PYTHON"] = "/root/anaconda3/bin/python"

    # os.environ['SPARK_CLASSPATH'] = "file:///C:/Users/Administrator/Desktop/postgresql-9.4.1211.jar"
    # sparkClassPath = os.getenv('SPARK_CLASSPATH', 'file:/C:/Users/Administrator/Desktop/postgresql-9.4.1211.jar')
    conf = SparkConf()
    conf.setMaster("myspark://ambari1:7077") \
        .setAppName("PythonWordCount") \
        .set("myspark.executor.cores", 1) \
        .set("myspark.cores.max", 4)
        # .set('myspark.jars', 'file:%s' % sparkClassPath) \
        # .set('myspark.executor.extraClassPath', sparkClassPath) \
        # .set('myspark.driver.extraClassPath', sparkClassPath)
    spark = SparkSession \
        .builder \
        .config(conf=conf) \
        .getOrCreate()


    jdbcDF = spark.read \
        .format("jdbc") \
        .option("url", "jdbc:postgresql://10.10.62.3:5432/dip_db_v1_1") \
        .option("dbtable", "dm_component") \
        .option("user", "postgres") \
        .option("password", "root") \
        .load()
    jdbcDF.createTempView("component")
    jdbcDF.show()
    jdbcWDF = spark.sql("select name from component")
    jdbcWDF.write \
        .format("jdbc") \
        .option("url", "jdbc:postgresql://10.10.62.3:5432/dip_db_v1_1") \
        .option("dbtable", "spark_test") \
        .option("user", "postgres") \
        .option("password", "root") \
        .save(mode="append")
