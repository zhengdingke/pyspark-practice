from pyspark.sql import SparkSession
from pyspark.sql import Window
from pyspark.sql.functions import monotonically_increasing_id

spark = SparkSession \
    .builder \
    .master("local") \
    .appName("KMeansExample") \
    .getOrCreate()
w = Window.orderBy("id")
df = spark.createDataFrame([(1, "John Doe", 21), (2, "John", 23), (3, "Doe", 22)], ("age", "name", "height"))
df = df.orderBy("height")
df = df.withColumn("id", monotonically_increasing_id())
df.show()