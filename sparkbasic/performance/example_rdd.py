import sys
from operator import add
import os

from pyspark.sql import SparkSession
from pyspark.conf import SparkConf
import pandas as pd
os.environ["PYSPARK_PYTHON"] = "/root/anaconda3/bin/python"

if __name__ == '__main__':
    conf = SparkConf()
    conf.setMaster("spark://10.10.62.4:7077") \
    .setAppName("PythonWordCount") \
    .set("spark.executor.cores",3)  \
    .set("spark.cores.max",6) \
    .set("spark.eventLog.enabled", "true") \
    .set("spark.eventLog.dir", "hdfs://10.10.62.4:8020/spark/applicationHistory")

    spark = SparkSession \
        .builder\
        .config(conf=conf) \
        .getOrCreate()

    dataRdd = spark.read.text("hdfs://10.10.62.4:8020/dip/b.txt").rdd
    spark.read.parquet
    counts = dataRdd.map(lambda x: (x, 1)) \
                  .reduceByKey(add)
    output = counts.collect()
    for (word, count) in output:
        print("%s: %i" % (word, count))