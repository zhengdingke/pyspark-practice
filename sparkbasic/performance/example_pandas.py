import sys
from operator import add
import os
from pyspark.sql.functions import rand
from pyspark.sql import SparkSession
from pyspark.conf import SparkConf
import pandas as pd
import numpy as np
from pyspark.sql.functions import rand, randn,floor
from pyspark.sql.functions import pandas_udf, PandasUDFType

os.environ["PYSPARK_PYTHON"] = "/root/anaconda3/bin/python"

if __name__ == '__main__':
    conf = SparkConf()
    conf.setMaster("spark://10.10.62.4:7077") \
        .setAppName("python_example_pandas") \
        .set("spark.executor.cores", 3) \
        .set("spark.cores.max", 6) \
        .set("spark.eventLog.enabled", "true") \
        .set("spark.eventLog.dir", "hdfs://ambari1:8020/spark/applicationHistory")

    spark = SparkSession \
        .builder \
        .config(conf=conf) \
        .getOrCreate()
    # spark.conf.set("spark.sql.execution.arrow.enabled", "true")
    pdf = pd.DataFrame(np.random.rand(100, 3))
    df = spark.createDataFrame()
    df = spark.range(20000000).toDF("row").drop("row") \
        .withColumn("id", floor(rand() * 10000)).withColumn("spent", (randn() + 3) * 100)

    @pandas_udf("id long, spent double", PandasUDFType.GROUPED_MAP)
    def subtract_mean(pdf):
        spent = pdf.spent
        return pdf.assign(spent=spent - spent.mean())

    df_to_pandas_arrow = df.groupby("id").apply(subtract_mean).show()