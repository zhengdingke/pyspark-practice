import sys
from operator import add
import os

from pyspark.sql import SparkSession
from pyspark.conf import SparkConf
import pandas as pd
os.environ["PYSPARK_PYTHON"] = "/root/anaconda3/bin/python"

if __name__ == '__main__':
    conf = SparkConf()
    conf.setMaster("spark://10.10.62.4:7077") \
    .setAppName("PythonWordCount") \
    .set("spark.executor.cores",3)  \
    .set("spark.cores.max",6) \
    .set("spark.eventLog.enabled", "true") \
    .set("spark.eventLog.dir", "hdfs://ambari1:8020/spark/applicationHistory")

    spark = SparkSession \
        .builder\
        .config(conf=conf) \
        .getOrCreate()

    data = spark.read.text("hdfs://10.10.62.4:8020/dip/b.txt")
    count = data.selesct("value").count
    print("count:"+str(count));