
from operator import add

from pyspark.sql import SparkSession
from pyspark.conf import SparkConf

if __name__ == "__main__":
    spark = SparkSession\
        .builder\
        .master("myspark://192.168.3.131:7077") \
        .appName("lazyExample")\
        .getOrCreate()

    sc = spark.sparkContext
    dataRdd = spark.sparkContext.parallelize(["Alina","Tom","Sky","Blue"])
    print("para operation end")
    mapRdd = dataRdd.map(lambda x: (x, 1))
    print("map operation end")
    count = mapRdd.reduceByKey(add).collect()
    print("count operation end")
