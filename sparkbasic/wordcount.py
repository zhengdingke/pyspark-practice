import sys
from operator import add
import os
from pyspark.sql import SparkSession
from pyspark.conf import SparkConf

if __name__ == '__main__':
    os.environ["PYSPARK_PYTHON"] = "/root/anaconda3/bin/python"
    conf = SparkConf()
    conf.setMaster("spark://10.10.62.4:7077") \
    .setAppName("PythonWordCount") \
    .set("spark.executor.cores",3)  \
    .set("spark.cores.max",6) \
    .set("spark.eventLog.enabled", "true") \
    .set("spark.eventLog.dir", "hdfs://ambari1:8020/spark/applicationHistory")


    spark = SparkSession \
        .builder\
        .config(conf=conf) \
        .getOrCreate()

    dataRdd = spark.sc.parallelize(["Ali_na","To_m","Sk_y","Blu_e","Ali_na","To_m","Sk_y","Blu_e","Ali_na","To_m","Sk_y","Blu_e"
                                              ,"Ali_na","To_m","Sk_y","Blu_e","Ali_na","To_m","Sk_y","Blu_e","Ali_na","To_m","Sk_y","Blu_e"
                                              ,"Ali_na","To_m","Sk_y","Blu_e","Ali_na","To_m","Sk_y","Blu_e"])
    counts = dataRdd.map(lambda x: (x, 1)) \
                  .reduceByKey(add)
    output = counts.collect()
    for (word, count) in output:
        print("%s: %i" % (word, count))

    spark.stop()