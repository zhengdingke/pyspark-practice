# pyspark-practice


#### 项目简介
pyspark 实践


#### 目录结构
- `bin` 脚本文件

- `data` 测试数据

- `jar` 

- `pythonbasic` python基础语法代码

- `sparkbasic` pyspark基础算子实例

- `sparkmlib` pysparkmlib实例和预测服务


