from pyspark.ml.feature import VectorAssembler
from pyspark.ml.wrapper import JavaWrapper
from pyspark.sql import DataFrame

from pyspark_db_utils import ComponentSparkSession

coss = ComponentSparkSession()
spark = coss.spark
train = spark.read.format("csv") \
    .option("inferSchema", "true") \
    .option("header", "true") \
    .load("../data/mllib/iris_train.csv")

test = spark.read.format("csv") \
    .option("inferSchema", "true") \
    .option("header", "true") \
    .load("../data/mllib/iris_test.csv")
LABEL = 'label'
STR_LABEL = 'class'
FEATURES = 'features'
features = [c for c in train.columns if c not in [STR_LABEL,LABEL]]
assembler = VectorAssembler(inputCols=features, outputCol=FEATURES)

train = assembler.transform(train).select(FEATURES, LABEL)
test = assembler.transform(test).select(FEATURES, LABEL)
# xgb_params = {
#     "eta": 0.1, "eval_metric": "rmse",
#     "gamma": 0, "max_depth": 5, "min_child_weight": 1.0,
#     "objective": "reg:squarederror", "seed": 0,
#     # xgboost4j only
#     "num_round": 100, "num_early_stopping_rounds": 10,
#     "maximize_evaluation_metrics": False,
#     "num_workers": 1, "use_external_memory": False,
#     "missing": np.nan,
# }
xgb_params = {
    "eval_metric": "rmse",
    "max_depth": 5,
    "objective": "reg:squarederror",
    # xgboost4j only
    "num_round": 100,
    "num_workers": 1
}
scala_map = spark._jvm.PythonUtils.toScalaMap(xgb_params)
# set evaluation set
# eval_set = {'eval': test._jdf}
# scala_eval_set = myspark._jvm.PythonUtils.toScalaMap(eval_set)
# logger.info('training')
j = JavaWrapper._new_java_obj(
    "ml.dmlc.xgboost4j.scala.myspark.XGBoostRegressor", scala_map) \
    .setFeaturesCol(FEATURES).setLabelCol(LABEL)
    # .setEvalSets(scala_eval_set)
jmodel = j.fit(train._jdf)

# print_summary(jmodel)
#
# # get validation metric
preds = jmodel.transform(test._jdf)

pred = DataFrame(preds, spark)
# #
pred.show()
