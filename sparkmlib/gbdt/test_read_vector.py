from pyspark.ml.feature import VectorAssembler
from pyspark.ml.linalg import VectorUDT, Vectors
from pyspark.sql.functions import udf

from pyspark_db_utils import ComponentSparkSession

coss = ComponentSparkSession()

rdf = coss.getData('"admin"."10000203_0_1"')

rdf.show()
print(rdf.dtypes)

vector_columns = [k for (k,v) in rdf.dtypes if v.find("array")==-1]
print(vector_columns)




# to_vector = udf(lambda vs: Vectors.dense(vs), VectorUDT())
# mto = rdf.withColumn("hour",to_vector(rdf["hour"]))
# mto.show()
# mto.printSchema()
#
# # schema = StructType([StructField("features", VectorUDT(), True)])
# # row = Row("features")
# # ddf = result.map(lambda x: row(DenseVector(x))).toDF(schema)
#
# vecAss = VectorAssembler(inputCols=["hour"], outputCol='features')
# df_va = vecAss.transform(mto)
# df_va.show()
