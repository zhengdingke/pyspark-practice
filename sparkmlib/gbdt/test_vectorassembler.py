from pyspark.ml.feature import VectorAssembler
from pyspark.ml.linalg import DenseVector, SparseVector, VectorUDT, Vectors
from pyspark.ml.regression import GBTRegressor
from pyspark.sql.functions import udf
from pyspark.sql.types import ArrayType, BinaryType, ByteType, StructType, StructField, DoubleType, FloatType, \
    IntegerType, LongType, StringType,_need_converter
from pyspark_db_utils import ComponentSparkSession
coss = ComponentSparkSession()

dataset = coss.spark.createDataFrame(
  [(0, 18, 1.0, Vectors.dense(0.0, 10.0, 0.5), 1.0)]
).toDF("id", "hour", "mobile", "userFeatures", "clicked")

assembler = VectorAssembler() \
  .setInputCols(["hour", "mobile", "userFeatures"]) \
  .setOutputCol("features")

voutput = assembler.transform(dataset)
voutput.select("features", "clicked").show()
