import pandas as pd
import numpy as np
from scipy import sparse
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import LabelBinarizer
from sklearn.preprocessing import MultiLabelBinarizer

testdata = pd.DataFrame({'pet': ['cat', 'dog', 'dog', 'fish'],'age': [4 , 6, 3, 3],'salary':[4, 1, 1, 1]})
#这里我们把 pet、age、salary 都看做类别特征，所不同的是 age 和 salary 都是数值型，而 pet 是字符串型。我们的目的很简单: 把他们全都二值化，进行 one-hot 编码.
#sklearn 的新版本中，OneHotEncoder 的输入必须是 2-D array，而 testdata.age 返回的 Series 本质上是 1-D array，所以要改成2-D array"""

a1 = OneHotEncoder(sparse = False).fit_transform( testdata[['age']] )
a2 = OneHotEncoder(sparse = False).fit_transform( testdata[['salary']])
final_output = sparse.hstack((a1,a2))

print("success")
