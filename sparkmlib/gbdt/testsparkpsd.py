from pyparsing import col
from pyspark import Row
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.feature import StringIndexer, OneHotEncoderEstimator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.linalg import DenseVector, SparseVector, VectorUDT, Vectors
from pyspark.ml.regression import GBTRegressor
from pyspark.sql.functions import udf
from pyspark.sql.types import ArrayType, DoubleType
from pyspark_db_utils import ComponentSparkSession
from operator import attrgetter

coss = ComponentSparkSession()
data = coss.spark.read.format("csv")\
    .option("inferSchema", "true") \
    .option("header", "true") \
    .load("../data/psd/TRAIN_30.csv")
data.createTempView("raw")

model_train = coss.spark.sql("select * from raw where date_format(raw.date, 'yyyy-MM-dd') >'2019-01-01'")

model_train_rename = model_train.withColumnRenamed("in_hour","hour").withColumnRenamed("in_minute","minute").withColumnRenamed("in_weekday","weekday")
# model_train_rename.show()
model_train_rename.printSchema()

# encoder = OneHotEncoderEstimator(inputCols=["stationID", "hour","minute"],
#                                  outputCols=["stationID_index", "hour_index","minute_index"])
# model = encoder.fit(model_train_rename)
# encoded = model.transform(model_train_rename)
# encoded.show()


def multi_column_OneHotEncoder(df, columns):
    columns_index = []
    for column in columns:
        columns_index.append(column + "_index")
    encoder = OneHotEncoderEstimator(inputCols=columns,outputCols=columns_index)
    tdf = encoder.fit(df).transform(df)

    for column in columns:
        tdf = tdf.drop(column).withColumnRenamed(column + "_index",column)
    return tdf

# 对标签进行标准化，将字符串映射成数字
def multi_column_StringEncoder(df, columns):
    si = StringIndexer()
    for column in columns:
        si.setInputCol(column)
        si.setOutputCol(column + "_index")
        df = si.fit(df).transform(df)
        df = df.drop(column).withColumnRenamed(column + "_index",column)
    return df

# def multi_column_OneHotEncoder1(df, columns):
#     si = OneHotEncoder()
#     for column in columns:
#         si.setInputCol(column)
#         si.setOutputCol(column + "_index")
#         df = si.transform(df)
#         df = df.drop(column).withColumnRenamed(column + "_index", column)
#     return df

def feature_converter(df,columns):
    vecAss = VectorAssembler(inputCols=columns, outputCol='features')
    df_va = vecAss.transform(df)
    return df_va

model_train_sindex = multi_column_StringEncoder(model_train_rename,['stationID', 'hour', 'minute'])
#
# model_train_sindex.show()

model_train_onehoter = multi_column_OneHotEncoder(model_train_sindex,['stationID', 'hour', 'minute'])
# model_train_onehoter.show()
# model_train_onehoter.printSchema()
# model_train_onehoter = model_train_onehoter.withColumn("hour",model_train_onehoter.hour.astype("string"))
# model_train_onehoter.withColumn("hour",model_train_onehoter["hour"].cast(ByteType()))
# model_train_onehoter.show(1)
# model_train_onehoter.printSchema()
# print(model_train_onehoter.dtypes)
# schema = StructType([
# StructField("innums", LongType(), True),
# StructField("hour", ArrayType(StringType()), True)
# ])
# finalRdd= model_train_onehoter.rdd.map(lambda row:(row.inNums,list(row.hour.toArray()))).collect()
# mrdd = finalRdd
#
# list=[]
# for f in finalRdd:
#     list.append(f)
#     print(f)
# newdf = coss.myspark.createDataFrame(list,schema)
# newdf.show()
# outputs = {}
# outputs['data_out'] = '10000203_0_1'
# # coss.writeData(newdf,outputs['data_out'])
#
# # finalRdd= model_train_onehoter.select("hour").rdd.map(attrgetter("hour")).foreach(print)
to_array = udf(lambda s: [float(v) for v in list(s.toArray())], ArrayType(DoubleType()))
mto = model_train_onehoter.withColumn("hour",to_array(model_train_onehoter["hour"]))
mto.show(30)
# coss.writeData(mto,outputs['data_out'])


# finalRdd.map(lambda f:udf(f)).foreach(print)

# schema = StructType([
# StructField("innums", LongType(), True),
# StructField("hour", ArrayType(DoubleType()), True)
# ])
# frdd = model_train_onehoter.rdd.map(lambda row:(row.inNums,row.hour.toArray())).map(lambda row:(row.inNums,[float(v) for v in row])).foreach(print)
# newdf = coss.myspark.createDataFrame(frdd,schema)
# newdf.show()


# model_train_onehoter.select("hour").rdd.map(lambda row:row.vector.toArray().toList()).foreach(print)

train_data, test_data= feature_converter(model_train_onehoter,['stationID', 'hour', 'minute','weekday', 'inNums_mean', 'is_weekday']).randomSplit([7.0, 3.0], 101)
train_data.show()
# # # 模型训练
label = "inNums"
gbt = GBTRegressor(featuresCol="features",labelCol=label)
gbt_model = gbt.fit(train_data)
# gbt_model.write().overwrite().save("../model")
#
# # 模型预测
result = gbt_model.transform(test_data)
result.show(3)
#
# # 模型评估
# gbt_evaluator = RegressionEvaluator(labelCol='inNums', metricName="rmse", predictionCol='prediction')
# rmse = gbt_evaluator.evaluate(result)
# print('测试数据的均方根误差（rmse）:{}'.format(rmse))



