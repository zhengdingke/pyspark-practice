from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.feature import StringIndexer, OneHotEncoderEstimator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import GBTRegressor
from pyspark.ml.feature import VectorAssembler
from pyspark.sql.types import StructType, StructField, ArrayType, FloatType, IntegerType, LongType, DoubleType, Row

from pyspark_db_utils import ComponentSparkSession
from operator import attrgetter
from pyspark.ml.linalg import *

coss = ComponentSparkSession()

# model_train_rename.show()
data = coss.spark.read.format("csv")\
    .option("inferSchema", "true") \
    .option("header", "true") \
    .load("../data/psd/TRAIN_30.csv")
data.createTempView("raw")

model_train = coss.spark.sql("select * from raw where date_format(raw.date, 'yyyy-MM-dd') >'2019-01-01'")

model_train_rename = model_train.withColumnRenamed("in_hour","hour").withColumnRenamed("in_minute","minute").withColumnRenamed("in_weekday","weekday")
# model_train_rename.show()
model_train_rename.printSchema()

# encoder = OneHotEncoderEstimator(inputCols=["stationID", "hour","minute"],
#                                  outputCols=["stationID_index", "hour_index","minute_index"])
# model = encoder.fit(model_train_rename)
# encoded = model.transform(model_train_rename)
# encoded.show()


def multi_column_OneHotEncoder(df, columns):
    columns_index = []
    for column in columns:
        columns_index.append(column + "_index")
    encoder = OneHotEncoderEstimator(inputCols=columns,outputCols=columns_index)
    tdf = encoder.fit(df).transform(df)

    for column in columns:
        tdf = tdf.drop(column).withColumnRenamed(column + "_index",column)
    return tdf

# 对标签进行标准化，将字符串映射成数字
def multi_column_StringEncoder(df, columns):
    si = StringIndexer()
    for column in columns:
        si.setInputCol(column)
        si.setOutputCol(column + "_index")
        df = si.fit(df).transform(df)
        df = df.drop(column).withColumnRenamed(column + "_index",column)
    return df
model_train_sindex = multi_column_StringEncoder(model_train_rename,['stationID', 'hour', 'minute'])
#
model_train_sindex.show()

model_train_onehoter = multi_column_OneHotEncoder(model_train_sindex,['stationID', 'hour', 'minute'])
model_train_onehoter.show()
# df = coss.myspark.sparkContext.parallelize([
#     (SparseVector(3, [0, 2], [1.0, 3.0]), ),
#     (SparseVector(3, [1], [4.0]), )
# ]).toDF(["features"])
# schema = StructType([
# StructField("features", ArrayType(IntegerType()), True)
# ])
schema = StructType([
    StructField("id", LongType(), True),
    StructField("name", ArrayType(DoubleType()), True)
])
list=[]
list.append((228, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]))
list.append((228, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]))

data1=coss.spark.createDataFrame(list,schema)
data1.show()
# data=coss.myspark.createDataFrame([Row(array=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
#                                  Row(array=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])])
outputs = {}
outputs['data_out'] = '10000204_0_1'
coss.writeData(data1,outputs['data_out'])
# df = coss.myspark.sparkContext.parallelize([
#     (SparseVector(3, [0, 2], [1.0, 3.0]), ),
#     (SparseVector(3, [1], [4.0]), )
# ]).toDF(["features"])

# features = df.rdd.map(attrgetter("features")).collect()
# def as_matrix(vec):
#     data, indices = vec.values, vec.indices
#     shape = 1, vec.size
#     return SparseVector()

# for f in features:
#     print(f)
#     print(f.toArray())
#     print(f.indices)
#     print(f.values)
# n=3
# # inputCols=["features[{0}]".format(i) for i in range(n)]
# inputCols=(df["features"].getItem(i) for i in range(n))
# print(inputCols)
# assembler = VectorAssembler(
#     inputCols=["features[{0}]".format(i) for i in range(n)],
#     outputCol="features_vector")
#
# assembler.transform(df.select(
#     "*", *(df["features"].getItem(i) for i in range(n))
# ))

# tuple(row.vector.toArray().tolist())


# (SparseVector(3, [1], [4.0])