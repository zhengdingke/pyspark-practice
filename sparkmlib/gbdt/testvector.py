from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.feature import StringIndexer, OneHotEncoderEstimator
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import GBTRegressor
from pyspark.ml.feature import VectorAssembler
from pyspark.sql.types import StructType, StructField, ArrayType, FloatType, IntegerType, LongType, DoubleType, Row

from pyspark_db_utils import ComponentSparkSession
from operator import attrgetter
from pyspark.ml.linalg import *

coss = ComponentSparkSession()

# df = coss.myspark.sparkContext.parallelize([
#     (SparseVector(3, [0, 2], [1.0, 3.0]), ),
#     (SparseVector(3, [1], [4.0]), )
# ]).toDF(["features"])
# schema = StructType([
# StructField("features", ArrayType(IntegerType()), True)
# ])
schema = StructType([
    StructField("id", LongType(), True),
    StructField("name", ArrayType(DoubleType()), True)
])
list=[]
list.append((228, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]))
list.append((228, [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0]))

data1=coss.spark.createDataFrame(list,schema)
data1.show()
# data=coss.myspark.createDataFrame([Row(array=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]),
#                                  Row(array=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])])
outputs = {}
outputs['data_out'] = '10000204_0_1'
coss.writeData(data1,outputs['data_out'])
# df = coss.myspark.sparkContext.parallelize([
#     (SparseVector(3, [0, 2], [1.0, 3.0]), ),
#     (SparseVector(3, [1], [4.0]), )
# ]).toDF(["features"])

# features = df.rdd.map(attrgetter("features")).collect()
# def as_matrix(vec):
#     data, indices = vec.values, vec.indices
#     shape = 1, vec.size
#     return SparseVector()

# for f in features:
#     print(f)
#     print(f.toArray())
#     print(f.indices)
#     print(f.values)
# n=3
# # inputCols=["features[{0}]".format(i) for i in range(n)]
# inputCols=(df["features"].getItem(i) for i in range(n))
# print(inputCols)
# assembler = VectorAssembler(
#     inputCols=["features[{0}]".format(i) for i in range(n)],
#     outputCol="features_vector")
#
# assembler.transform(df.select(
#     "*", *(df["features"].getItem(i) for i in range(n))
# ))

# tuple(row.vector.toArray().tolist())


# (SparseVector(3, [1], [4.0])