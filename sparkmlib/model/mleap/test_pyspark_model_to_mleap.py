from mleap import pyspark

from pyspark.ml.linalg import Vectors
from mleap.pyspark.spark_support import SimpleSparkSerializer
from pyspark.ml.feature import VectorAssembler, StandardScaler, OneHotEncoder, StringIndexer,Binarizer
from pyspark.ml import Pipeline, PipelineModel
from pyspark.ml.regression import LinearRegression
from pyspark.ml.classification import LogisticRegression
import os
from pyspark.sql import SparkSession
from pyspark.conf import SparkConf
from unittest import TestCase


class TestPySparkModelToMleap(TestCase):
    def setUp(self):
        conf = SparkConf()
        conf.setMaster("local")
        self.spark = SparkSession.builder.config(conf=conf).getOrCreate()
        self.spark.sparkContext.setLogLevel('INFO')

    def tearDown(self):
        self.spark.stop()

    # pipeline多模型
    def test_generate_pipeline_model(self):
        df = self.spark.read.csv(os.path.join(os.path.dirname(__file__), "../resources/spark-demo.csv"), header=True,
                                 inferSchema=True)
        df = df.withColumn("test_double", df["test_double"].cast("double"))
        stringIndexer = StringIndexer(inputCol="test_string",outputCol="test_index")
        binarizer = Binarizer(threshold=0.5,inputCol="test_double",outputCol="test_bin")
        pipeline = Pipeline(stages=[stringIndexer,binarizer])
        pipelineModel = pipeline.fit(df)
        pipelineModel.serializeToBundle("jar:file:/pyspark.lr.zip", pipelineModel.transform(df))

    # pipeline单模型
    def test_generate_single_model(self):
        df = self.spark.read.format("libsvm") \
            .load("../resources/sample_linear_regression_data.txt")
        lr = LinearRegression(maxIter=10, regParam=0.3, elasticNetParam=0.8)
        pipeline = Pipeline(stages=[lr])
        pipelineModel = pipeline.fit(df)
        pipelineModel.serializeToBundle("jar:file:/pyspark1.lr.zip", pipelineModel.transform(df))


