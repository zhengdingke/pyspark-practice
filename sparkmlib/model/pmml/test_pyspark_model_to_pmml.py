from py4j.java_gateway import JavaObject
from pyspark.context import SparkContext
from pyspark.ml import Pipeline
from pyspark.ml.classification import DecisionTreeClassifier
from pyspark.ml.feature import RFormula
from pyspark.sql import SQLContext
from pyspark2pmml import PMMLBuilder
from pyspark.sql import SparkSession
from pyspark.conf import SparkConf
import os
from unittest import TestCase


class TestPySparkModelToPmml(TestCase):

    def setUp(self):
        conf = SparkConf()
        conf.setMaster("local")
        self.spark = SparkSession.builder.config(conf=conf).getOrCreate()
        self.spark.sparkContext.setLogLevel('INFO')
        #测试数据
        self.df = self.spark.read.csv(os.path.join(os.path.dirname(__file__), "../resources/Iris.csv"), header=True,
                                 inferSchema=True)

    def tearDown(self):
        self.spark.stop()

    #使用spark第三方pyspark2pmml
    def test_pipeline(self):
        formula = RFormula(formula="Species ~ .")
        classifier = DecisionTreeClassifier()
        pipeline = Pipeline(stages=[formula, classifier])
        pipelineModel = pipeline.fit(self.df)
        pmmlBuilder = PMMLBuilder(self.spark.sparkContext, self.df, pipelineModel) \
            .verify(self.df.sample(False, 0.1))
        pmmlBuilder = pmmlBuilder.putOption(classifier, "compact", True)
        compactPmmlPath = pmmlBuilder.buildFile("pyspark2pmml.pmml")
