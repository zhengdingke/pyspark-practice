from collections import OrderedDict
from functools import wraps
import flask
import json
import logging
import numpy as np
import pandas as pd
import time
from json import JSONEncoder
from six import reraise
from flask import Response
from sparkmlib.predict.exceptions import SparkServiceException,Error_Code_To_Http_Status
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

CONTENT_TYPE_CSV = "text/csv"
CONTENT_TYPE_JSON = "application/json"
CONTENT_TYPE_JSON_RECORDS_ORIENTED = "application/json; format=pandas-records"
CONTENT_TYPE_JSON_SPLIT_ORIENTED = "application/json; format=pandas-split"
CONTENT_TYPE_JSON_SPLIT_NUMPY = "application/json-numpy-split"

CONTENT_TYPES = [
    CONTENT_TYPE_CSV,
    CONTENT_TYPE_JSON,
    CONTENT_TYPE_JSON_RECORDS_ORIENTED,
    CONTENT_TYPE_JSON_SPLIT_ORIENTED,
    CONTENT_TYPE_JSON_SPLIT_NUMPY
]
_logger = logging.getLogger(__name__)
def _handle_serving_error(error_message, error_code):
    """
        服务异常处理
    """
    reraise(SparkServiceException(
                error_msg=error_message,
                error_code=error_code))

def parse_json_input(json_input, orient="split"):
    """
        解析json格式为pd dataframe
    """
    try:
        return pd.read_json(json_input, orient=orient, dtype=False)
    except Exception:
        _handle_serving_error(
            error_message=(
                "Failed to parse input as a Pandas DataFrame. please using the `pandas.DataFrame.to_json(..., orient='{orient}')`"
                " method.".format(orient=orient)),
            error_code=Error_Code_To_Http_Status.BAD_REQUEST)


def parse_csv_input(csv_input):
    """
        解析csv文件为pd dataframe
    """
    try:
        return pd.read_csv(csv_input)
    except Exception:
        _handle_serving_error(
            error_message=(
                "Failed to parse input as a Pandas DataFrame. please using the `pandas.DataFrame.to_csv()` method."),
            error_code=Error_Code_To_Http_Status.BAD_REQUEST)


def parse_split_oriented_json_input_to_numpy(json_input):
    try:
        json_input_list = json.loads(json_input, object_pairs_hook=OrderedDict)
        return pd.DataFrame(index=json_input_list['index'],
                            data=np.array(json_input_list['data'], dtype=object),
                            columns=json_input_list['columns']).infer_objects()
    except Exception:
        _handle_serving_error(
            error_message=(
                "Failed to parse input as a Numpy. please using the `pandas.DataFrame.to_json(..., orient='split')` method."
            ),
            error_code=Error_Code_To_Http_Status.BAD_REQUEST)

def init(model):
    """
    模型服务初始化
    """
    app = flask.Flask(__name__)
    @app.route('/ping', methods=['GET'])
    def ping():
        health = model is not None
        status = 200 if health else 404
        return flask.Response(response='\n', status=status, mimetype='application/json')

    @app.route('/predict', methods=['POST'])
    @catch_spark_service_exception
    def transformation():
        a = time.time()
        if flask.request.content_type == CONTENT_TYPE_CSV:
            data = flask.request.data.decode('utf-8')
            csv_input = StringIO(data)
            data = parse_csv_input(csv_input=csv_input)
        elif flask.request.content_type in [CONTENT_TYPE_JSON, CONTENT_TYPE_JSON_SPLIT_ORIENTED]:
            data = parse_json_input(json_input=flask.request.data.decode('utf-8'),
                                    orient="split")
        elif flask.request.content_type == CONTENT_TYPE_JSON_RECORDS_ORIENTED:
            data = parse_json_input(json_input=flask.request.data.decode('utf-8'),
                                    orient="records")
        elif flask.request.content_type == CONTENT_TYPE_JSON_SPLIT_NUMPY:
            data = parse_split_oriented_json_input_to_numpy(flask.request.data.decode('utf-8'))
        else:
            return flask.Response(
                response=("This predict only supports the following content types,"
                          " {supported_content_types}. Got '{received_content_type}'.".format(
                            supported_content_types=CONTENT_TYPES,
                            received_content_type=flask.request.content_type)),
                status=415,
                mimetype='text/plain')

        try:
            raw_predictions = model.predict(data)
        except Exception:
            _handle_serving_error(
                error_message=(
                    "Encountered an unexpected error while evaluating the model. Verify"
                    " that the serialized input Dataframe is compatible with the model for"
                    " inference."),
                error_code=Error_Code_To_Http_Status.BAD_REQUEST)
        result = StringIO()
        predictions_to_json(raw_predictions, result)
        print("预测耗费时间", time.time() - a)
        return flask.Response(response=result.getvalue(), status=200, mimetype='application/json')

    return app

def predictions_to_json(raw_predictions, output):
    """
    预测结果转换为json
    """
    predictions = _get_jsonable_obj(raw_predictions, pandas_orient="records")
    json.dump(predictions, output, cls=NumpyEncoder)

def _get_jsonable_obj(data, pandas_orient="records"):
    if isinstance(data, np.ndarray):
        return data.tolist()
    if isinstance(data, pd.DataFrame):
        return data.to_dict(orient=pandas_orient)
    if isinstance(data, pd.Series):
        return pd.DataFrame(data).to_dict(orient=pandas_orient)
    else:
        return data

class NumpyEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, np.generic):
            return np.asscalar(o)
        return JSONEncoder.default(self, o)

def catch_spark_service_exception(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except SparkServiceException as e:
            response = Response(mimetype='application/json')
            response.set_data(e.serialize_as_json())
            response.status_code = e.get_http_status_code()
            return response
    return wrapper

def load_model(model_path):
    """
    模型加载
    """
    import pyspark
    spark = pyspark.sql.SparkSession._instantiatedSession
    if spark is None:
        spark = pyspark.sql.SparkSession.builder.config("spark.sql.execution.arrow.enabled","true") \
            .master("local").getOrCreate()
            # .master("local[1]").getOrCreate()
            # .master("spark://ambari2:7077").getOrCreate()
        spark.sparkContext.setLogLevel('INFO')
    return _PyFuncModelWrapper(spark, _load_model(model_path))

def _load_model(model_path):
    """
        模型加载
    """
    from pyspark.ml.pipeline import PipelineModel
    model = PipelineModel.load(model_path)
    return model

class _PyFuncModelWrapper(object):
    def __init__(self, spark, spark_model):
        self.spark = spark
        self.spark_model = spark_model

    def predict(self, pandas_df):
        spark_df = self.spark.createDataFrame(pandas_df)
        return self.spark_model.transform(spark_df).select("prediction").toPandas()["prediction"].values