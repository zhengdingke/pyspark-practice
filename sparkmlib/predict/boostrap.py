import sys
import os
sys.path.append("/user/pyspark")
from sparkmlib.predict import spark_predict_service
# os.environ["PYSPARK_PYTHON"] = "/root/anaconda3/bin/python"

model = spark_predict_service.load_model("hdfs://10.10.62.4:8020/dip/data/ml/")
app = spark_predict_service.init(model)
app.run(host="0.0.0.0",port=5001,debug=True,use_reloader=False)

