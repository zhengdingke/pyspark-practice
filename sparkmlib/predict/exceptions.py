import json
import enum

class  Error_Code_To_Http_Status(enum.Enum):
    INTERNAL_ERROR = 500
    BAD_REQUEST = 400
    INVALID_PARAMETER_VALUE = 400


class SparkServiceException(Exception):
    def __init__(self, error_msg, error_code=Error_Code_To_Http_Status.INTERNAL_ERROR):
        super().__init__(self)
        self.error_msg = error_msg
        self.error_code = error_code.value

    def serialize_as_json(self):
        exception_dict = {'error_code': self.error_code.value, 'error_msg': self.message}
        return json.dumps(exception_dict)

    def get_http_status_code(self):
        return 500
