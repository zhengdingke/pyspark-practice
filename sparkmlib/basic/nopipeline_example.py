#

"""
Pipeline Example.
"""

# $example on$
from pyspark.ml import Pipeline
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.feature import HashingTF, Tokenizer
# $example off$
from pyspark.sql import SparkSession

if __name__ == "__main__":
    spark = SparkSession\
        .builder\
        .master("myspark://192.168.3.131:7077") \
        .appName("PipelineExample")\
        .getOrCreate()

    # $example on$
    # Prepare training documents from a list of (id, text, label) tuples.
    training = spark.createDataFrame([
        (0, "a b c d e myspark", 1.0),
        (1, "b d", 0.0),
        (2, "myspark f g h", 1.0),
        (3, "hadoop mapreduce", 0.0)
    ], ["id", "text", "label"])

    # Configure an ML pipeline, which consists of three stages: tokenizer, hashingTF, and lr.
    tokenizer = Tokenizer(inputCol="text", outputCol="words")
    tdf = tokenizer.transform(training).collect()
    # hashingTF = HashingTF(inputCol=tokenizer.getOutputCol(), outputCol="features")
    # hdf = hashingTF.transform(tdf)
    # lr = LogisticRegression(maxIter=10, regParam=0.001)
    # # pipeline = Pipeline(stages=[tokenizer, hashingTF, lr])
    #
    # # Fit the pipeline to training documents.
    # model = lr.fit(hdf)
    # # model = pipeline.fit(training)
    #
    # # Prepare test documents, which are unlabeled (id, text) tuples.
    # test = myspark.createDataFrame([
    #     (4, "myspark i j k"),
    #     (5, "l m n"),
    #     (6, "myspark hadoop myspark"),
    #     (7, "apache hadoop")
    # ], ["id", "text"])
    #
    # # Make predictions on test documents and print columns of interest.
    # prediction = model.transform(test)
    # selected = prediction.select("id", "text", "probability", "prediction")
    # for row in selected.collect():
    #     rid, text, prob, prediction = row
    #     print("(%d, %s) --> prob=%s, prediction=%f" % (rid, text, str(prob), prediction))
    # # $example off$

    spark.stop()
